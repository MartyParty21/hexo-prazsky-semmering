---
title: Hostivice
layout: zastavka
ordering: 70
details:
  request: false
  tracks: 9
  altitude: 357
  gps: [50.0839, 14.2583]
  picture: hostivice.jpg
  picture_cc: 'ŠJů, <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.en">CC BY-SA 4.0</a>'
  picture_url: "https://commons.wikimedia.org/wiki/File:Hostivice,_n%C3%A1dra%C5%BE%C3%AD_(01).jpg"
---

> Hostivické nádraží je železniční uzel na tratích č. 120 (Praha–Rakovník), 121 (Hostivice–Podlešín) a 122 (Praha-Smíchov – Hostivice – Rudná u Prahy).
>
> Nádraží vzniklo roku 1862 na trase původní Lánské koněspřežky (budova čp. 90).
> Po vybudování odbočky tehdy již Buštěhradské dráhy do Prahy-Smíchov (v provozu od 3. března 1872) získalo nádraží novou podobu (budova čp. 93 a další stavby). 
> Na přelomu let 2010/2011 prošla staniční budova hostivického nádraží několikaměsíční rekonstrukcí.
>
> — [Wikipedia](https://cs.wikipedia.org/wiki/Hostivice_(n%C3%A1dra%C5%BE%C3%AD%29)

